#!/bin/sh
# Author: Marvin Vek <laen@onedot.nl>

# Always save /var/tmp* in case the battery dies during suspend
/etc/local.d/99var_tmp.stop
# Lock the screen (requiring root to resume) and suspend to memory
DISPLAY=:0 /usr/bin/slock echo -n mem > /sys/power/state
