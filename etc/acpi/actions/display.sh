#!/bin/sh
# Author: Marvin Vek <laen@onedot.nl>

# jack/videoout: this is _only_ triggered if the external display goes to sleep
# 		 there is no event at all unplugging the USB-C cable if the external display is _not_ the power source

case $* in
	button/lid\ LID\ open|video/switchmode\ VMOD\ *)
		if xrandr --display :0 | grep -q '^DP-1 connected'; then
			xrandr --display :0 --output eDP-1 --auto --output DP-1 --primary --auto --above eDP-1
		else
			xrandr --display :0 --output eDP-1 --primary --auto --output DP-1 --off
		fi
		killall -SIGCONT -g chrome
		;;
	button/lid\ LID\ close)
		if xrandr --display :0 | grep -q '^DP-1 connected'; then
			xrandr --display :0 --output eDP-1 --off --output DP-1 --primary --auto
		else
			source ${0%/*}/suspend.sh
		fi
		;;
	jack/videoout\ VIDEOOUT\ plug)
		killall -SIGCONT -g chrome
		;;
	jack/videoout\ VIDEOOUT\ unplug)
		killall -SIGSTOP -g chrome
		;;
esac
exit
