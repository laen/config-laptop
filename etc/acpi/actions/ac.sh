#!/bin/sh
# Author: Marvin Vek <laen@onedot.nl>

# switching all windows to the laptop on unplugging the USB-C cable doesn't happen in a display-event
# switching the windows back may fail due to a delay in the monitor announcement, just do it manually

case $* in
	ac_adapter\ ACPI0003:00\ 00000080\ 00000000)
		if xrandr --display :0 --listactivemonitors | grep -q "eDP-1"; then
			xrandr --display :0 --output eDP-1 --primary --auto --output DP-1 --off
			killall -SIGCONT -g chrome
		else
			source ${0%/*}/suspend.sh
		fi
		;;
	ac_adapter\ ACPI0003:00\ 00000080\ 00000001)
		if xrandr --display :0 | grep -q '^DP-1 connected'; then
			xrandr --display :0 --output eDP-1 --auto --output DP-1 --primary --auto --above eDP-1
			killall -SIGCONT -g chrome
		fi
esac
