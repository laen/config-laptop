#!/bin/sh
# Author: Marvin Vek <laen@onedot.nl>
#
# Arguments:
# + for brightnessup, - for brightnessdown
#
# The intel_backlight subsystem provices a max_brightness of 24242 on the Thinkpad X1C7.
# This results in the following clean divisions:
# 17 steps of 1426
# 23 steps of 1054
# 31 steps of  782
# 34 steps of  713
# 46 steps of  527
# 62 steps of  391

read -r _BRT </sys/class/backlight/intel_backlight/brightness
echo -n $(($_BRT - $(($_BRT % 527)) $1 527)) > /sys/class/backlight/intel_backlight/brightness
