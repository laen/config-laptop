#!/bin/sh
# Author: Marvin Vek <laen@onedot.nl>

# Toggle first rfkill type match using soft value and consider it done
for _KILL in /sys/class/rfkill/rfkill?; do
	read -r _TYPE < $_KILL/type
	[[ $_TYPE != "$1" ]] && continue
	read -r _SOFT < $_KILL/soft
	[[ $_SOFT == "0" ]] && rfkill block "$1" || rfkill unblock "$1"
	break
done
