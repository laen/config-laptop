#!/bin/sh

# $XDG_CACHE_HOME defines the base directory relative to which user specific
# non-essential data files should be stored. If $XDG_CACHE_HOME is either not
# set or empty, a default equal to $HOME/.cache should be used.
export XDG_CACHE_HOME=/var/tmp/$USER
if [ ! -d "$XDG_CACHE_HOME" ]; then
	mkdir -m 0700 -p $XDG_CACHE_HOME >/dev/null 2>&1 || unset XDG_CACHE_HOME
fi
export HISTFILE=${XDG_CACHE_HOME:-$HOME/.cache}/bash_history
export LESSHISTFILE=${XDG_CACHE_HOME:-$HOME/.cache}/less_history

# $XDG_CONFIG_HOME defines the base directory relative to which user-specific
# configuration files should be stored. If $XDG_CONFIG_HOME is either not
# set or empty, a default equal to $HOME/.config should be used.
export XDG_CONFIG_HOME=$HOME/.config
if [ ! -d "$XDG_CONFIG_HOME" ]; then
	mkdir -m 0750 -p $XDG_CONFIG_HOME >/dev/null 2>&1 || unset XDG_CONFIG_HOME
fi

# $XDG_DATA_HOME defines the base directory relative to which user-specific
# data files should be stored. If $XDG_DATA_HOME is either not set or empty,
# a default equal to $HOME/.local/share should be used.
export XDG_DATA_HOME=$HOME/.local/share
if [ ! -d "$XDG_DATA_HOME" ]; then
	mkdir -m 0750 -p $XDG_CONFIG_HOME >/dev/null 2>&1 || unset XDG_DATA_HOME
fi

# XDG_RUNTIME_DIR defines the base directory relative to which user-specific
# non-essential runtime files and other file objects (such as sockets, named
# pipes, ...) should be stored. The directory MUST be owned by the user, and he
# MUST be the only one having read and write access to it. Its Unix # access
# mode MUST be 0700.
export XDG_RUNTIME_DIR=/run/user/$UID
if [ "$(id -u)" -eq "0" -a ! -d "${XDG_RUNTIME_DIR%/*}" ]; then
	mkdir -m 0753 -p ${XDG_RUNTIME_DIR%/*}
fi
if [ ! -d "$XDG_RUNTIME_DIR" ]; then
	mkdir -m 0700 -p $XDG_RUNTIME_DIR >/dev/null 2>&1 || unset XDG_RUNTIME_DIR
fi
