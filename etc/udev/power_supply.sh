#!/bin/sh
# Power supply class changes (expects either 0 or 1, uses 0 by default)
if [ "${1:-0}" = "0" ]; then
	# Intel P-State driver 
	for _CGOV in /sys/devices/system/cpu/cpufreq/policy?/scaling_governor; do echo 'powersave' > $_CGOV; done
	for _CEPP in /sys/devices/system/cpu/cpufreq/policy?/energy_performance_preference; do echo 'balance_power' > $_CEPP; done
else
	# Intel P-State driver 
	for _CGOV in /sys/devices/system/cpu/cpufreq/policy?/scaling_governor; do echo 'performance' > $_CGOV; done
	for _CEPP in /sys/devices/system/cpu/cpufreq/policy?/energy_performance_preference; do echo 'balance_performance' > $_CEPP; done
fi
