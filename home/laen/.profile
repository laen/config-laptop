# This file should be considered to be sourced by login shells only, actions
# here may not be inherited by (interactive) non-login shells.

# Environment variables
export PATH=$PATH:$HOME/.local/bin
export PASSWORD_STORE_DIR=$HOME/vcs/gitlab.com/laen/pass

# https://www.gnupg.org/documentation/manuals/gnupg/Agent-Examples.html
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
	export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
gpg-connect-agent updatestartuptty /bye &>/dev/null

# Test for an interactive shell. There is no need to set anything past this
# point for scp and rcp, and it's important to refrain from outputting anything
# in those cases.
if [[ $- != *i* ]]; then
	return
fi

# Use the same colors in the virtual consoles as virtual terminals.
if [ "$TERM" = "linux" ] && [ -r "$HOME/.Xresources" ]; then
	sed '/^*color/!d; s/*color//; s/:#/ /' \
		$HOME/.Xresources 2>/dev/null | xargs printf '\e]P%X%s'
fi

# This file is sourced by bash for login shells. The following line runs your
# .bashrc and is recommended by the bash info pages.
if [ "$SHELL" = "/bin/bash" ] && [ -r "$HOME/.bashrc" ]; then
	. $HOME/.bashrc
fi
